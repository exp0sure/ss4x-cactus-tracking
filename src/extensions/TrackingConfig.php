<?php
/**
 * User: Carsten
 * Date: 28/01/2022
 * Silverstripe 4.x
 * SiteConfig extension to add Tracking codes. Default available: Google Analytics, Piwik
 * Activate extension in /mysite/_config/extensions.yml
 * If tracking codes are set in CMS, tracking scripts will be embedded on main page template
 * ToDo: Validation GA if one field is empty
 * ToDo: Validation if Analytics Async Code, Analytics Code and Tag Manager code contain opening/closing <script> tags and matching amount (onBeforeWrite? If not return with error?)
 */
use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

class TrackingConfig extends DataExtension {
    private static $db = array(
        'GoogleAnalyticsAsyncCode'  => 'Text',              // placed after <body>
        'GoogleAnalyticsCode'       => 'Text',              // will be deferred
        'GoogleTagManagerCode'      => 'Text',              // placed after <body>
        'GoogleSiteVerification'    => 'Varchar(128)',      // Meta Tag
        'FacebookPixel'             => 'Text',              // placed after <body>
        'MatomoCode'                => 'Text',              // placed after <body>
    );

    public function updateCMSFields(FieldList $fields) {
        $fields->addFieldToTab("Root.Tracking", new LiteralField('CustomCode1','<h3 style="font-weight: bold;">Google Tracking (best to use either Analytics or Tag Manager)</h3>'));     // Inserts a literal field with custom code

        $fields->addFieldToTab("Root.Tracking",
            $googleanalyticsasync = new TextField('GoogleAnalyticsAsyncCode', 'Analytics Async Code')
        );
        $googleanalyticsasync->setAttribute('placeholder','<script async>...</script>');
        $googleanalyticsasync->setRightTitle('Paste async <script> segment for <header> here.');

        $fields->addFieldToTab("Root.Tracking",
            $googleanalytics = new TextareaField("GoogleAnalyticsCode", "Analytics Other Code")
        );
        $googleanalytics->setRightTitle('Place second <script> segment of Analytics code here to optimise render-blocking during page load (faster)')
                        ->setAttribute('placeholder','<script>...</script>');

        $fields->addFieldToTab("Root.Tracking",
            $googletagmanager = new TextareaField("GoogleTagManagerCode", "Tag Manager Code")
        );
        $googletagmanager->setRightTitle('Code for <head>; AND <body> section')
                         ->setAttribute('placeholder','<noscript>...<script> <noscript>...</noscript>');

        $fields->addFieldToTab("Root.Tracking", new LiteralField('CustomCode2','<br><br><h3 style="font-weight: bold;">Google Site Owner Verification</h3>'));

        $fields->addFieldToTab("Root.Tracking",
            $googleverification = new TextField("GoogleSiteVerification", "Site Verification Code")
        );
        $googleverification->setRightTitle('For various Google services like Search Console etc. this can also be done through Analytics or Tag Manager - if available.');


        $fields->addFieldToTab("Root.Tracking", new LiteralField('CustomCode3','<h3 style="font-weight: bold;">Facebook Tracking</h3>'));          // Inserts a literal field with custom code

        $fields->addFieldToTab("Root.Tracking",
            $facebookpixel = new TextareaField("FacebookPixel", "Facebook Pixel Code")
        );


        $fields->addFieldToTab("Root.Tracking", new LiteralField('CustomCode4','<br><br><h3 style="font-weight: bold;">Matomo Tracking</h3>'));

        $fields->addFieldToTab("Root.Tracking",
            $matomocode = new TextareaField("MatomoCode", "Matomo Code")
        );
    }
}