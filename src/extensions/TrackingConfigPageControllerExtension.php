<?php
use SilverStripe\Core\Extension;
use SilverStripe\View\Requirements;
use Silverstripe\SiteConfig\SiteConfig;

class TrackingConfigPageControllerExtension extends Extension
{
    /**
     * Injects part of Google Analytics code at page end
     * Content of "Analytics Other Code" (GoogleAnalyticsCode)
     */
    public function onAfterInit() {
        $config = SiteConfig::current_site_config();
        if(!empty($config->GoogleAnalyticsCode) && !empty($config->GoogleAnalyticsAsyncCode)) {                         // Only include if both code parts are set
            $bPageEndJsForced = Requirements::get_force_js_to_bottom();

            if(!$bPageEndJsForced) {
                Requirements::set_force_js_to_bottom(true);                                                             // If JS not forced to page end, switch it on
            }

            // Include Google Analytics code inline if set
            // Don't change indentation, no spaces after opening and closing terminators, heredoc syntax!
            Requirements::customScript(<<<JS
                $config->GoogleAnalyticsCode
JS
            );

            if(!$bPageEndJsForced) {
                Requirements::set_force_js_to_bottom(false);                                                            // If JS wasn't forced to page end, switch back off
            }
        }
    }

    /**
     * Check if all needle(s) are contained in a haystack
     * @param $aNeedles array 1-dimensional array with needles (strings)
     * @param $sHaystack string to search in for needles
     * @return boolean
     */
    public function checkAllTags($aNeedles, $sHaystack){
        $aResults = array();

        foreach ($aNeedles as $sNeedle){
            $aResults[] = strpos($sHaystack, $sNeedle);
        }

        if (in_array(false, $aResults, true)) {
            return false;
        }
        else {
            return true;
        }
    }

    /**
    * Assemble JS for existing tracking codes ad print
    * Insert $CactusTrackingConfigCodes right after <body> tag in master template
    *
    * If no <script> or <noscript> tags are present in the tracking codes, the include will be commented
    * out with an alert comment in the source code to prevent breaking the website layout
    */
    public function CactusTrackingConfigCodes(){
        $sJS = "";
        $config = SiteConfig::current_site_config();

        if(!empty($config->GoogleAnalyticsAsyncCode) && !empty($config->GoogleAnalyticsCode)){
            $sJS .= ( $this->checkAllTags(["<script>", "</script>"],$config->GoogleAnalyticsAsyncCode) || $this->checkAllTags(["<noscript>", "</noscript>"],$config->GoogleAnalyticsAsyncCode) ? $config->GoogleAnalyticsAsyncCode : "<!-- ALERT: Google Analytics Async Code missing script/noscript tags -->");
            $sJS .= "\r\n";
        }

        if(!empty($config->GoogleTagManagerCode)){
            $sJS .= ( $this->checkAllTags(["<script>", "</script>"],$config->GoogleTagManagerCode) || $this->checkAllTags(["<noscript>", "</noscript>"],$config->GoogleTagManagerCode) ? $config->GoogleTagManagerCode : "<!-- ALERT: GoogleTagManagerCode missing script/noscript tags -->");
            $sJS .= "\r\n";
        }

        if(!empty($config->FacebookPixel)) {
            $sJS .= ( $this->checkAllTags(["<script>", "</script>"],$config->FacebookPixel) || $this->checkAllTags(["<noscript>", "</noscript>"],$config->FacebookPixel) ? $config->FacebookPixel : "<!-- ALERT: Facebook Pixel missing script/noscript tags -->");
            $sJS .= "\r\n";
        }

        if(!empty($config->MatomoCode)){
            $sJS .= ( $this->checkAllTags(["<script>", "</script>"],$config->MatomoCode) || $this->checkAllTags(["<noscript>", "</noscript>"],$config->MatomoCode) ? $config->MatomoCode : "<!-- ALERT: Matomo Code missing script/noscript tags -->");
            $sJS .= "\r\n";
        }

        return $sJS;
    }
}
