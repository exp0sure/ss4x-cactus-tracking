# SS 4.x Cactus-Tracking v2.3
This will add another Tab to your SiteConfig where the code for Google Analytics, Google Tag Manager, Google Site Owner Verification, Facebook Pixel and Matomo can be added  
Find it in the CMS under ```Settings > Tracking```  

###Configuration  
#### Tracking Code
Cactus SiteConfig extension for tracking codes  
Add the variable for tracking code injection directly after the `<body>` tag in your main template:
```{$CactusTrackingConfigCodes.RAW}```

#### Google Site Ownership
Add Google Site Verification meta tag to template in `<head>` section (e.g. Page.ss)
```
<% if $SiteConfig.GoogleSiteVerification %>
  <meta name="google-site-verification" content="{$SiteConfig.GoogleSiteVerification}">
<% end_if %>
```

*To Do:*  
* Maybe inject Google Site Verification directly into `<head>` MetaTags instead of adding it manually to the template - requires extending MetaTags function? 